package com.bourd0n.healthyday.model;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@SuppressWarnings("unused")
public interface RecordRepository extends JpaRepository<Record, Long> {

    List<Record> findAllByTime(Integer time);

}
