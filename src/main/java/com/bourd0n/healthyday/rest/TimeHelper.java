package com.bourd0n.healthyday.rest;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class TimeHelper {

    public static final List<Integer> ALL_AVAILABLE_TIMES = Collections.unmodifiableList(new ArrayList<Integer>() {{
        for (int i = 10; i < 18; i++) {
            for (int j = 0; j < 60; j = j + 15) {
                add(i * 60 + j);
            }
        }
    }});

    public Integer getMinutesFromTime(String time) {
        String[] split = time.split(":");
        Integer hours = Integer.valueOf(split[0]);
        Integer minutes = Integer.valueOf(split[1]);

        if (hours < 9 || hours > 18) {
            throw new IllegalStateException("Hours should be between 9 and 18. Passed: " + hours);
        }

        if (minutes < 0 || minutes > 60 || minutes % 15 != 0) {
            throw new IllegalStateException("Illegal minutes: " + minutes);
        }

        return hours * 60 + minutes;
    }

    public String getTimeFromMinutes(Integer timeInMin) {
        Integer hours = timeInMin / 60;
        Integer minutes = timeInMin % 60;
        return String.format("%02d:%02d", hours, minutes);
    }
}
