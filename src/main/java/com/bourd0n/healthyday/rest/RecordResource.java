package com.bourd0n.healthyday.rest;

import com.bourd0n.healthyday.dto.RecordDto;
import com.bourd0n.healthyday.model.Record;
import com.bourd0n.healthyday.model.RecordRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class RecordResource {

    private final RecordRepository recordRepository;

    private final TimeHelper timeHelper;

    @Inject
    public RecordResource(RecordRepository recordRepository, TimeHelper timeHelper) {
        this.recordRepository = recordRepository;
        this.timeHelper = timeHelper;
    }

    @RequestMapping(value = "/records",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Record> createRecord(@Valid @RequestBody RecordDto recordDto) throws URISyntaxException {
        Record record = new Record();
        Integer timeInMinutes = timeHelper.getMinutesFromTime(recordDto.getTime());
        record.setTime(timeInMinutes);
        record.setName(recordDto.getSurname() + " " + recordDto.getName()
                + (recordDto.getMiddleName() == null ? "" : " " + recordDto.getMiddleName()));
        record.setCreated(LocalDateTime.now());
        synchronized (this) {
            List<Record> allByTime = recordRepository.findAllByTime(timeInMinutes);
            if (allByTime.size() >= 1) {
                throw new IllegalStateException("Извините, это время уже занято. Попробуйте записаться на другое время.");
            }
            Record result = recordRepository.save(record);
            return ResponseEntity.created(new URI("/api/records/" + result.getId()))
                    .body(result);
        }
    }

    @RequestMapping(value = "/records/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Record> getRecord(@PathVariable Long id) {
        Record record = recordRepository.findOne(id);
        return Optional.ofNullable(record)
                .map(result -> new ResponseEntity<>(result, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @RequestMapping(value = "/records",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Record>> getAllRecords() {
        List<Record> records = recordRepository.findAll();
        return new ResponseEntity<>(records, HttpStatus.OK);
    }

    @RequestMapping(value = "/times", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getAllAvailableTimes() {
        List<Record> all = recordRepository.findAll();
        Map<Integer, Integer> countsForTimes = new HashMap<>();
        List<Integer> availableTimes = new ArrayList<>(TimeHelper.ALL_AVAILABLE_TIMES);
        all.forEach(record -> {
            Integer time = record.getTime();
            countsForTimes.compute(time, (key, oldValue) -> oldValue == null ? 1 : oldValue + 1);
        });
        countsForTimes.entrySet().stream()
                .filter(entry -> entry.getValue() >= 1)
                .forEach(entry -> availableTimes.remove(entry.getKey()));
        List<String> times = availableTimes.stream().
                map(timeHelper::getTimeFromMinutes)
                .collect(Collectors.toList());
        return new ResponseEntity<>(times, HttpStatus.OK);
    }
}
