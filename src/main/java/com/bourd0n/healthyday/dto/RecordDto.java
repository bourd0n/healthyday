package com.bourd0n.healthyday.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

public class RecordDto implements Serializable {


    private Long id;
    /**
     * hh:mm
     */
    @NotNull
    private String time;

    @NotNull
    private String surname;

    @NotNull
    private String name;

    private String middleName;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecordDto recordDto = (RecordDto) o;
        return Objects.equals(id, recordDto.id) &&
                Objects.equals(time, recordDto.time) &&
                Objects.equals(surname, recordDto.surname) &&
                Objects.equals(name, recordDto.name) &&
                Objects.equals(middleName, recordDto.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, time, surname, name, middleName);
    }

    @Override
    public String toString() {
        return "RecordDto{" +
                "id=" + id +
                ", time='" + time + '\'' +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                '}';
    }
}
