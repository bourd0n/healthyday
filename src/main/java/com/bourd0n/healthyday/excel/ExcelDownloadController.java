package com.bourd0n.healthyday.excel;

import com.bourd0n.healthyday.model.Record;
import com.bourd0n.healthyday.model.RecordRepository;
import com.bourd0n.healthyday.rest.TimeHelper;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.util.List;

@Controller
public class ExcelDownloadController {

    private final static Logger log = LoggerFactory.getLogger(ExcelDownloadController.class);

    private final RecordRepository recordRepository;
    private final TimeHelper timeHelper;

    @Inject
    public ExcelDownloadController(RecordRepository recordRepository, TimeHelper timeHelper) {
        this.recordRepository = recordRepository;
        this.timeHelper = timeHelper;
    }

    @RequestMapping(value = "/admin/excel/records", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response) throws IOException {

        File file = getExcelWorkBook();

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            log.info("mimetype is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        response.setContentType(mimeType);

        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
        response.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");


        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
        //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));

        response.setContentLength((int) file.length());

        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

        //Copy bytes from source to destination(outputstream in this example), closes both streams.
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }


    private File getExcelWorkBook() {
        try {
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet();
            List<Record> records = recordRepository.findAll();

            final HSSFRow firstRow = sheet.createRow(0);

            firstRow.createCell(0).setCellValue("id");
            firstRow.createCell(1).setCellValue("ФИО");
            firstRow.createCell(2).setCellValue("Время записи");
            firstRow.createCell(3).setCellValue("Время регистрации записи");

            for (Record record : records) {
                final int lastRowNum = sheet.getLastRowNum();
                final HSSFRow row = sheet.createRow(lastRowNum + 1);

                row.createCell(0).setCellValue(record.getId());
                row.createCell(1).setCellValue(record.getName());
                row.createCell(2).setCellValue(timeHelper.getTimeFromMinutes(record.getTime()));
                row.createCell(3).setCellValue(record.getCreated().toString());
            }

            String fileName = "allrecords.xls";
            FileOutputStream fileOut = new FileOutputStream(fileName);
            workbook.write(fileOut);
            fileOut.close();

            return new File(fileName);
        } catch (IOException e) {
            throw new RuntimeException("Exception occurred during saving to excel file", e);
        }
    }
}
