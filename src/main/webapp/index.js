$(document).ready(function () {

        $.getJSON("api/times", function (data) {
            $("#time").empty();
            if (data.length === 0) {
                bootbox.dialog({
                    title: "Запись закончена",
                    message: "Извините, к сожалению свободного времени для записи больше не осталось :(",
                    buttons: {
                        success: {
                            label: "Жаль:(",
                            className: "btn-warning"
                        }
                    }
                });
                $('#submit').prop('disabled', true);
                return;
            }
            $.each(data, function () {
                $("#time").append('<option value="' + this + '">' + this + '</option>');
            });
        });


        $('.form-validation').submit(function (event) {
            var surnameVar = $.trim($('#surname').val());
            var nameVar = $.trim($('#name').val());
            var timeVar = $('#time').find('option:selected').text();
            var incorrectData = false;
            event.preventDefault();
            if (surnameVar.length === 0) {
                var errorField = $('.form-input-surname-row');
                errorField.addClass('form-invalid-data');
                errorField.find('.form-invalid-data-info').text('Пожалуйста, введите фамилию');
                incorrectData = true;
                // event.preventDefault();
            } else {
                var successField = $('.form-input-surname-row');
                successField.removeClass('form-invalid-data');
                successField.addClass('form-valid-data');
            }
            if (nameVar === '') {
                var errorField = $('.form-input-name-row');
                errorField.addClass('form-invalid-data');
                errorField.find('.form-invalid-data-info').text('Пожалуйста, введите имя');
                // event.preventDefault();
                incorrectData = true;
            } else {
                var successField = $('.form-input-name-row');
                successField.removeClass('form-invalid-data');
                successField.addClass('form-valid-data');
            }

            if (incorrectData) {
                return;
            }
            var data = JSON.stringify({
                time: timeVar,
                name: nameVar,
                surname: surnameVar
            });
            $.ajax({
                    url: "api/records",
                    type: "POST",
                    data: data,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var message = 'Информация о записи:<br/>' +
                            'Время: ' + timeVar + '<br/>' +
                            'Пациент: ' + surnameVar + ' ' + nameVar + '<br/>' +
                            '<br/>' +
                            'Спасибо!';
                        bootbox.dialog({
                            title: "Запись сохранена!",
                            message: message,
                            buttons: {
                                success: {
                                    label: "Ок",
                                    className: "btn-success",
                                    callback: function () {
                                        location.reload();
                                    }
                                }
                            }
                        });
                    },
                    error: function (errorData) {
                        jsonError = jQuery.parseJSON(errorData.responseText);
                        var errorMessage = jsonError.message;
                        bootbox.dialog({
                            title: "Ошибка!",
                            message: errorMessage,
                            buttons: {
                                success: {
                                    label: "Ок",
                                    className: "btn-danger",
                                    callback: function () {
                                        location.reload();
                                    }
                                }
                            }
                        });
                    }
                }
            );
        });
    }
);

function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    return !!filter.test(sEmail);
};

function saveRecord() {

}